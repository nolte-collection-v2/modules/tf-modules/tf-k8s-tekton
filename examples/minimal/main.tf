
resource "kubernetes_namespace" "namespace" {
  metadata {
    name = "acc-cluster-tasks"
  }
}

module "install" {
  source    = "../../modules/wrapper"
  namespace = kubernetes_namespace.namespace.metadata[0].name
}


output "helm_release" {
  value = module.install.release
}
