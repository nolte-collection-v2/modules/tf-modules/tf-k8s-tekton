module "install" {
  source = "../install"
}

# give Certsmanger Time to Work
resource "time_sleep" "wait_operator" {
  depends_on = [module.install]

  create_duration = "30s"
}

module "cluster_tasks" {
  depends_on = [time_sleep.wait_operator]
  source     = "../cluster_tasks"
  namespace  = var.namespace
}

output "release" {
  value = module.install.release
}
