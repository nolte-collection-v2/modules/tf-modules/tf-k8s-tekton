#variable "tekton_tasks" {
#  default = [
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/ansible-runner/0.1/ansible-runner.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/ansible-tower-cli/0.1/ansible-tower-cli.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/buildpacks/0.1/buildpacks.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/docker-build/0.1/docker-build.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/git-clone/0.2/git-clone.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/helm-conftest/0.1/helm-conftest.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/helm-upgrade-from-repo/0.1/helm-upgrade-from-repo.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/helm-upgrade-from-source/0.1/helm-upgrade-from-source.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/kaniko/0.1/kaniko.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/kubeconfig-creator/0.1/kubeconfig-creator.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/kubernetes-actions/0.1/kubernetes-actions.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/terraform-cli/0.1/terraform-cli.yaml",
#    "https://raw.githubusercontent.com/tektoncd/catalog/master/task/wget/0.1/wget.yaml",
#
#  ]
#}

#data "http" "operator_tektoncd" {
#  count = length(var.tekton_tasks)
#  url   = var.tekton_tasks[count.index]
#}
#
locals {
  tekton_own_tasks = [
    "${path.module}/files/tk-tasks-debug.yaml",
    "${path.module}/files/tk-tasks-unarchive.yaml",
    "${path.module}/files/tk-tasks-rm.yaml",
    "${path.module}/files/tk-tasks-chmod.yaml",
    "${path.module}/files/tk-pipe-prepare-tf-providers.yaml",
    "${path.module}/files/tk-tasks-minio-upload.yaml",
    "${path.module}/files/tk-tasks-minio-creator.yaml",
    "${path.module}/files/tk-pipe-test-upload.yaml", ##debug only

  ]
}

#resource "kubectl_manifest" "tekton_task_ansible" {
#  count     = length(data.http.operator_tektoncd)
#  yaml_body = replace(data.http.operator_tektoncd[count.index].body, "metadata:\n", "metadata:\n  namespace:  ${var.namespace}\n")
#}

resource "kubectl_manifest" "tekton_pipe_tf_providers" {
  count     = length(local.tekton_own_tasks)
  yaml_body = replace(file(local.tekton_own_tasks[count.index]), "metadata:\n", "metadata:\n  namespace:  ${var.namespace}\n")
}

