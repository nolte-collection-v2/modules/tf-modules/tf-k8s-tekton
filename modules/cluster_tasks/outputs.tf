
output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    kubectl_manifest.tekton_pipe_tf_providers,
  ]
}
