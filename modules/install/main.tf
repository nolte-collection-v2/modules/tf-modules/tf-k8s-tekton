

locals {
  tekton_manifests = split("---", file("${path.module}/files/v0.15.2-1.yaml"))
}

resource "kubectl_manifest" "operator_tektoncd" {
  count     = length(local.tekton_manifests)
  yaml_body = local.tekton_manifests[count.index]
}
